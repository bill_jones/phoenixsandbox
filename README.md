#Quick Start#

##Installing PostgreSQL##
###Ubuntu/Debian###

Install the database engine, client, and administration tools.

```shell
sudo apt-get install postgresql-client postgresql postgresql-contrib
sudo apt-get install pgadmin3
```
Connect to the database engine as the default "postgres" user.

```shell
sudo -u postgres psql postgres
```
Set a password for the "postgres" user.
```sql
\password postgres
```
This command will then prompt for a password.

Once the password has been set, close the connection with Control+D.

The next step will be to create our first database. This can be done with the
following command:
```shell
sudo -u postgres createdb mydb
```
This will create a new database, "mydb".

Finally to configure pgadmin3, you will have to install the admin pack.
```shell
sudo -u postgres psql
CREATE EXTENSION adminpack;
```
And change the default authentication mechanic from "peer" to "md5" in the
user configuration file, /etc/postgresql/9.3/main/pg_hba.conf.

So change the line:
```shell
# Database administrative login by Unix domain socket
local   all             postgres                                peer
```

to

```shell
# Database administrative login by Unix domain socket
local   all             postgres                                md5
```

Once the file has been saved, we will need to restart the database engine.
```shell
sudo /etc/init.d/postgresql reload
```

So finally, we are able to run the pgadmin3 command from a terminal and
connect to our database instance "mydb".
